<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;

class DesafioController extends Controller
{
    public function desafio1($id = 1)
    {
        $invoice = Invoice::find(1);

        $invoices = Invoice::whereHas('products',function ($q) {
            $q->where('quantity', '>=', 100);
        })->get();

        $products_selected = $invoice->products()->where('price', '>=', 1000000)->get();

        $products = Product::where('price', '>=', 1000000)->get();
        return view('desafio1.index', compact('invoice', 'invoices', 'products', 'products_selected'));
    }

    public function desafio3($id = 1)
    {
        $invoice = Invoice::find($id);
        return view('desafio3.index', compact('invoice'));
    }

    public function desafio3_nuevo($id)
    {
        Product::create([
            'invoice_id' => $id,
            'name' => 'Producto '.rand(1, 100000),
            'quantity' => rand(1, 100),
            'price' => rand(1, 1000)
        ]);
        return redirect()->route('desafio3', $id);
    }
}

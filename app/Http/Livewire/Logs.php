<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Log;
use Auth;

class Logs extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $comentario, $task_id, $user_id, $registrado;
    public $updateMode = false;
    public $task_selected, $task;

    public function mount()
    {
        $this->task_selected = request()->id;
        $this->task = Task::find(request()->id);
    }
    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.logs.view', [
            'logs' => Log::where('user_id', Auth::user()->id)
                ->where('task_id', $this->task_selected)
                ->where(function ($q) use ($keyWord){
                    $q->orWhere('comentario', 'LIKE', $keyWord);
                })
                ->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }

    private function resetInput()
    {
		$this->comentario = null;
		$this->task_id = null;
		$this->user_id = null;
		$this->registrado = null;
    }

    public function store()
    {
        $this->validate([
		    'comentario' => 'required'
        ]);

        Log::create([
			'comentario' => $this-> comentario,
			'task_id' => $this->task_selected,
			'user_id' => Auth::user()->id,
			'registrado' => date('Y-m-d')
        ]);

        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Log registrado correctamente.');
    }

    public function edit($id)
    {
        $record = Log::findOrFail($id);

        $this->selected_id = $id;
		$this->comentario = $record-> comentario;
		$this->task_id = $record-> task_id;
		$this->user_id = $record-> user_id;
		$this->registrado = $record-> registrado;

        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'comentario' => 'required',
		'task_id' => 'required',
		'user_id' => 'required',
		'registrado' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Log::find($this->selected_id);
            $record->update([
			'comentario' => $this-> comentario,
			'task_id' => $this-> task_id,
			'user_id' => $this-> user_id,
			'registrado' => $this-> registrado
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Log actualizado correctamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Log::where('id', $id);
            $record->delete();
        }
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Task;
use Auth;

class Tasks extends Component
{
    use WithPagination;

	protected $paginationTheme = 'bootstrap';
    public $selected_id, $keyWord, $title, $description, $user_id, $maximo;
    public $updateMode = false;
    public $users;

    public function mount()
    {
        $this->users = User::all();
    }

    public function render()
    {
		$keyWord = '%'.$this->keyWord .'%';
        return view('livewire.tasks.view', [
            'tasks' => Task::where('user_id', Auth::user()->id)
                ->where(function ($q) use ($keyWord){
                    $q->orWhere('title', 'LIKE', $keyWord)
                        ->orWhere('description', 'LIKE', $keyWord);
                })
                ->paginate(10),
        ]);
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
    }

    private function resetInput()
    {
		$this->title = null;
		$this->description = null;
		$this->user_id = null;
		$this->maximo = null;
    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
            'maximo' => 'required',
        ]);

        Task::create([
			'title' => $this-> title,
			'description' => $this-> description,
			'user_id' => $this-> user_id,
			'maximo' => $this-> maximo
        ]);

        $this->resetInput();
		$this->emit('closeModal');
		session()->flash('message', 'Tarea registrada correctamente.');
    }

    public function edit($id)
    {
        $record = Task::findOrFail($id);

        $this->selected_id = $id;
		$this->title = $record-> title;
		$this->description = $record-> description;
		$this->user_id = $record-> user_id;
		$this->maximo = $record-> maximo;

        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
		'title' => 'required',
		'description' => 'required',
		'user_id' => 'required',
		'maximo' => 'required',
        ]);

        if ($this->selected_id) {
			$record = Task::find($this->selected_id);
            $record->update([
			'title' => $this-> title,
			'description' => $this-> description,
			'user_id' => $this-> user_id,
			'maximo' => $this-> maximo
            ]);

            $this->resetInput();
            $this->updateMode = false;
			session()->flash('message', 'Tarea actualizada correctamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Task::where('id', $id);
            $record->delete();
        }
    }
}

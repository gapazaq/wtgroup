<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoices';

    protected $fillable = ['date','user_id','seller_id','type'];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'invoice_id', 'id');
    }

    public function seller()
    {
        return $this->hasOne('App\Models\Seller', 'id', 'seller_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}

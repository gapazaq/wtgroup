<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'invoice_id' => $this->faker->numberBetween(1, 2),
            'name' => $this->faker->sentence(2),
            'quantity' => $this->faker->numberBetween(1, 120),
            'price' => $this->faker->numberBetween(0.5, 1000500.5)
        ];
    }
}

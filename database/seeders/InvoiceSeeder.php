<?php

namespace Database\Seeders;

use App\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::create([
            'date' => date('Y-m-d'),
            'user_id' => 1,
            'seller_id' => 1
        ]);
        Invoice::create([
            'date' => date('Y-m-d'),
            'user_id' => 1,
            'seller_id' => 1
        ]);
    }
}

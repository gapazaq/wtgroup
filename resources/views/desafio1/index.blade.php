@extends('layouts.app')

@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h3 class="display-10">Desafio 1</h3>
        {{--<p class="lead"></p>--}}
    </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">1. Obtener precio total de la factura.</div>

                <div class="card-body">
                    <ul>
                        <li>ID Invoice: {{ $invoice->id }}</li>
                        <li class="text-danger">Precio Total: {{ $invoice->products->sum('price') }}</li>
                        <li>Total Productos: {{ $invoice->products->count() }}</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">2. Obtener todos id de las facturas que tengan productos con cantidad mayor a 100.</div>

                <div class="card-body">
                    @if($invoices->count() != 0)
                    <ul>
                        @foreach($invoices as $row)
                        <li>ID Invoice: {{ $row->id }}</li>
                        @endforeach
                    </ul>
                    @else
                        No hay productos de la factura con cantidad 100.
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">3. Obtener todos los nombres de los productos cuyo valor final sea superior a $1.000.000 CLP.</div>

                <div class="card-body">
                    <h6>Para los productos de la Factura seleccionada:</h6>
                    <ul>
                        @foreach($products_selected as $ps)
                            <li>Producto: {{ $ps->name }}</li>
                        @endforeach
                    </ul>
                    <h6>Para todos los productos:</h6>
                    <ul>
                        @foreach($products as $p)
                            <li>Producto: {{ $p->name }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h3 class="display-10">Desafio 1</h3>
        {{--<p class="lead"></p>--}}
    </div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Detalles de la Factura.</div>

                <div class="card-body">
                    <ul>
                        <li>ID Invoice: {{ $invoice->id }}</li>
                        <li class="text-danger">Precio Total: {{ $invoice->total }}</li>
                        <li>Total Productos: {{ $invoice->products->count() }}</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Productos</div>

                <div class="card-body">
                    Últimos Productos Agregados
                    @foreach($invoice->products()->orderBy('id', 'DESC')->take(2)->get() as $row)
                        <li>{{ $row->name }} (Precio: {{$row->price}})</li>
                    @endforeach
                    <a href="{{ route('desafio3_nuevo', $invoice->id) }}" class="btn btn-outline-primary">Agregar Nuevo Producto (Aleatorio)</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('title', __('Registros Log'))
<div>
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h3 class="display-10">{{ $task->title }}</h3>
        <p class="lead">{{ $task->description }}</p>
    </div>

    <div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4>Registros (Log)</h4>
						</div>
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar">
						</div>
						<div class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createDataModal">
                            Nuevo Registro
						</div>
					</div>
				</div>

				<div class="card-body">
						@include('livewire.logs.create')
						@include('livewire.logs.update')
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<th>ID</th>
								<th>Comentario</th>
								<th>Responsable</th>
								<th>Fecha</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($logs as $row)
							<tr>
								<td>{{ $row->id }}</td>
								<td>{{ $row->comentario }}</td>
								<td>{{ $row->user->name }}</td>
								<td>{{ $row->registrado }}</td>
								<td width="90">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cogs"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                            <a class="dropdown-item" onclick="confirm('¿Eliminar Registro de Log?\nNo podrá recuperarla posterriormente!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar</a>
                                        </div>
                                    </div>
								</td>
							@endforeach
						</tbody>
					</table>
					{{ $logs->links() }}
					</div>
				</div>
			</div>
</div>

@section('title', __('Mis Tareas'))
			<div class="card">
				<div class="card-header">
					<div style="display: flex; justify-content: space-between; align-items: center;">
						<div class="float-left">
							<h4>Mis Tareas</h4>
						</div>
						@if (session()->has('message'))
						<div wire:poll.4s class="btn btn-sm btn-success" style="margin-top:0px; margin-bottom:0px;"> {{ session('message') }} </div>
						@endif
						<div>
							<input wire:model='keyWord' type="text" class="form-control" name="search" id="search" placeholder="Buscar">
						</div>
						<div class="btn btn-sm btn-primary" data-toggle="modal" data-target="#createDataModal">
                            Nueva Tarea
						</div>
					</div>
				</div>

				<div class="card-body">
						@include('livewire.tasks.create')
						@include('livewire.tasks.update')
				<div class="table-responsive">
					<table class="table table-bordered table-sm">
						<thead class="thead">
							<tr>
								<th>ID</th>
								<th>Título</th>
								<th>Descripción</th>
								<th>Asignado</th>
								<th>Fecha Máxima</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($tasks as $row)
							<tr class="{{ (strtotime($row->maximo) < strtotime(date("d-m-Y")))?'table-danger':'' }}">
								<td>{{ $row->id }}</td>
								<td>{{ $row->title }}</td>
								<td>{{ $row->description }}</td>
								<td>{{ $row->user->name }}</td>
								<td>{{ $row->maximo }}</td>
								<td width="120">
                                    <div class="btn-group">
                                        <a href="{{ route('desafio5.logs', $row->id) }}" class="btn btn-outline-danger btn-sm">
                                            <i class="fa fa-list"></i>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cogs"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        <a data-toggle="modal" data-target="#updateModal" class="dropdown-item" wire:click="edit({{$row->id}})">
                                            <i class="fa fa-edit"></i> Editar
                                        </a>
                                        <a class="dropdown-item" onclick="confirm('¿Eliminar Tarea?\nNo podrá recuperarla posterriormente!')||event.stopImmediatePropagation()" wire:click="destroy({{$row->id}})"><i class="fa fa-trash"></i> Eliminar</a>
                                        </div>
                                    </div>
								</td>
							@endforeach
						</tbody>
					</table>
					{{ $tasks->links() }}
					</div>
				</div>
			</div>

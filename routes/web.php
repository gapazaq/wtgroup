<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DesafioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/desafio1/{id?}', [DesafioController::class, 'desafio1'])->name('desafio1');
Route::get('/desafio3/{id?}', [DesafioController::class, 'desafio3'])->name('desafio3');
Route::get('/desafio3_nuevo/{id}', [DesafioController::class, 'desafio3_nuevo'])->name('desafio3_nuevo');

Route::prefix('desafio5')->name('desafio5.')->middleware('auth')->group(function () {
    Route::view('tasks', 'livewire.tasks.index')->name('tasks');
    Route::view('logs/{id}', 'livewire.logs.index')->name('logs');
});

//Route Hooks - Do not delete//
